<?php
    global $base_root;
    $template_dir = $base_root . '/' . drupal_get_path('theme', 'abiwelltherapywebsite');

    $white_logo_url = $template_dir . '/images/logo-white.png';
?>

<div class="<?php print $classes;?>">

    <header class="header">

        <div class="header-inner">
            <?php
            if ($logo || $site_title) : ?>
                <?php
                if ($title) : ?>
                    <div class="site-title">
                        <a href="<?php print $front_page ?>">
                            <?php
                            if ($logo) : ?>
                                <img
                                    src="<?php print $logo ?>"
                                    alt="<?php print $site_name_and_slogan ?>"
                                    title="<?php print $site_name_and_slogan ?>"
                                />
                            <?php
                            endif; ?>
                        </a>
                    </div>
                <?php
                else : ?>
                    <h1 class="site-title">
                        <a href="<?php print $front_page ?>">
                            <?php
                            if ($logo) : ?>
                                <img
                                    src="<?php print $logo ?>"
                                    alt="<?php print $site_name_and_slogan ?>"
                                    title="<?php print $site_name_and_slogan ?>"
                                />
                            <?php
                            endif; ?>
                        </a>
                    </h1>
                <?php
                endif; ?>
            <?php
            endif; ?>

            <nav class="navigation">
                <div class="nav-control"></div>
                <?php
                if ($main_menu) : ?>
                    <?php print theme('links__system_main_menu', array(
                    'links' => $main_menu,
                    'attributes' => array(
                        'id' => 'main-menu-links',
                        'class' => array('menu'),
                    ),
                    'heading' => array(
                        'text' => t('Main menu'),
                        'level' => 'h2',
                        'class' => array('element-invisible'),
                    ),
                    )); ?>
                    <a name="t" id="t"></a>
                <?php
                endif; ?>
            </nav>

        </div>
    </header>

    <?php print render($page['highlighted']) ?>

    <div class="content-container">
        <?php print render($title_prefix); ?>
        <?php
        if ($title) : ?>
            <div class="title-container">
                <h1 class="title" id="page-title">
                    <?php print $title; ?>
                </h1>
            </div>
        <?php
        endif; ?>
        <?php print render($title_suffix); ?>

        <div class="content-wrapper">
            <?php print render($page['content']); ?>
            <?php print render($page['sidebar_second']); ?>
        </div>
    </div>

    <footer class="footer">
        <div class="footer-inner">
            <?php print render($page['footer']); ?>
        </div>
    </footer>

</div>