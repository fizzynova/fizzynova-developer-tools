<?php
function {project-system-name}_css_alter(&$css)
{
    unset($css[drupal_get_path('module', 'system').'/system.theme.css']);
    unset($css[drupal_get_path('module', 'system').'/system.menus.css']);
    unset($css[drupal_get_path('module', 'field').'/theme/field.css']);

    $path_field_collection = drupal_get_path('module', 'field_collection');
    unset($css[$path_field_collection .'/field_collection.theme.css']);
}

function {project-system-name}_preprocess_page(&$variables)
{
    $variables['no_title'] = false;
    if (isset($variables['node'])) {
        $node = $variables['node'];
        $node_type = $node->type;

        // Remove title for specific content types
        $no_title_content_types = array();
        if (in_array($node_type, $no_title_content_types)) {
            $variables['no_title'] = true;
            $variables['title'] = false;
            $variables['classes_array'][] = 'no-title';
        }

        // Remove regions for specific content types
        $remove_regions = array(
            'content_type' => array(
                'sidebar_first',
                'sidebar_second'
            ),
        );
        if (isset($remove_regions[$node_type])) {
            foreach ($remove_regions[$node_type] as $region) {
                $variables['page'][$region] = false;
            }
        }
    }

    // Check if there is any content allocated to these regions
    // and add an appropriate positive or negative class.
    //
    // Use this for hooking CSS styling into whether the page
    // has a sidebar, etc.
    $regions_to_check = array(
        'sidebar_first',
        'sidebar_second'
    );
    foreach ($regions_to_check as $r) {
        $r_class = str_replace('_', '-', $r);
        if (isset($variables['page'][$r]) && $variables['page'][$r]) {
            $variables['classes_array'][] = 'has-' . $r_class;
        } else {
            $variables['page'][$r] = false;
            $variables['classes_array'][] = 'no-' . $r_class;
        }
    }

    // Used for logo alt tag, etc.
    $variables['site_name_and_slogan'] = sprintf(
        '%1$s - %2$s',
        $variables['site_name'],
        $variables['site_slogan']
    );

}

function {project-system-name}_preprocess_block(&$variables)
{
    // Remove titles from specific bundles (e.g. nodeblock content types)
    $no_title_bundles = array(
        'block_highlight_picture_and_text',
        'block_bubbles_highlight',
    );
    if (isset($variables['elements']['#bundle'])) {
        $bundle = $variables['elements']['#bundle'];
        if (in_array($bundle, $no_title_bundles)) {
            $variables['block']->subject = false;
            $variables['classes_array'][] = 'no-title';
        }
        $variables['classes_array'][] = str_replace('_', '-', $bundle);
    }

    // Add class for delta (general system name for block)
    if (isset($variables['block']->delta)) {
        $delta = $variables['block']->delta;
        $variables['classes_array'][] = sprintf(
            'block-%1$s',
            str_replace('_', '-', $delta)
        );
    }
}

function {project-system-name}_preprocess_node(&$variables)
{
    $content_type = $variables['node']->type;
    $view_mode = $variables['view_mode'];

    // Add a node-title class to all node titles
    $variables['title_attributes_array']['class'][] = 'node-title';

    // enable a generic view mode template
    $variables['theme_hook_suggestions'][] = 'node__' . $view_mode;

    // Add theme hook suggestion suffixes to this array to add the
    // suffix to every existing theme hook suggestion.
    //
    // This is useful to quickly generate very descriptive suggestions.
    $ths_to_add = array();
    $ths_to_add[] = $view_mode; // Suggestion suffix for view modes
    foreach ($ths_to_add as $suffix) {
        foreach ($variables['theme_hook_suggestions'] as $suggestion) {
            $ths = $suggestion . '__' . $suffix;
            $variables['theme_hook_suggestions'][] = $ths;
        }
    }

    // Make specific regions available *within* specific content types.
    // Means that individual blocks can be rendered within content
    // rather than having to do the crazys with page templates.
    //
    // Regions exposed to content in this way will be available to the
    // template through the $region array.
    $content_type_block_map = array();
    $content_type_block_map['content_type'] = array('sidebar_second');
    $variables['region'] = array();
    if ($variables['page']) {
        if (isset($content_type_block_map[$content_type])) {
            foreach ($content_type_block_map[$content_type] as $region_key) {
                // Get the content for each region and add it to the $region variable
                if ($blocks = block_get_blocks_by_region($region_key)) {
                    $variables['region'][$region_key] = $blocks;
                } else {
                    $variables['region'][$region_key] = array();
                }
            }
        }
    }

}

function {project-system-name}_preprocess_taxonomy_term(&$variables)
{
    // Add a class to the taxonomy to style by view mode
    $view_mode = $variables['view_mode'];
    $variables['classes_array'][] = sprintf(
        'view-mode-%1$s',
        str_replace('_', '-', $view_mode)
    );
}
