#!/bin/bash

if ! fizzy-tools-location; then
    echo "Install the Fizzy Dev Tools to run install"
    exit 1
fi

if ! fizzy-project-name; then
    echo "Must be in project root to run install"
    exit 1
fi

if ! [[ -f "_project-properties" ]]; then
    echo "No project properties found. Run:"
    echo "      fizzy-project-generate-properties"
    echo "And then try again"
    exit 1
fi

if ! hash drush; then
    echo "Drush not installed"
    echo "Run:"
    echo "      fizzy-drupal-drush-install"
    echo "And then try again"
    exit 1
fi

PROJECTNAME=$(fizzy-project-name)
PROJECTNAMECLEAN=$(fizzy-project-name --clean)
SSL=0

source "./_project-properties"

DIR=$(pwd)
SRC=$DIR/src
LIB=$DIR/lib
TARGETROOT="/var/www/$PROJECTNAME"
TARGET="$TARGETROOT/public"
DRUPALRES="$(fizzy-tools-location)/drupal/res"

$SRC/extra-install

if ! mysql -u $DBUSER -p$DBPASS -h$DBHOST -ANe 'SELECT "HELP ME!!! IM TRAPPED IN A DATABASE"'; then
    echo
    echo
    echo "Could not connect to database. Make sure that's all setup and"
    echo "working before attempting an install."
    echo
    echo "Used these DB credentials:"
    cat _project-properties
    echo
    echo "Do you need to run this command on the desired DB server?"
    echo
    fizzy-mysql-setup-command
    echo
    exit 1
fi

if [[ $SSL == 1 ]]; then
    cp $DRUPALRES/drupal-nginx-ssl /tmp/$PROJECTNAME-drupal-nginx
else
    cp $DRUPALRES/drupal-nginx /tmp/$PROJECTNAME-drupal-nginx
fi
sed -i "s/{project-web-server-name}/$WEBSERVERNAME/g" /tmp/$PROJECTNAME-drupal-nginx
sed -i "s/{project-system-name}/$PROJECTNAME/g" /tmp/$PROJECTNAME-drupal-nginx
sudo cp /tmp/$PROJECTNAME-drupal-nginx /etc/nginx/sites-available/$PROJECTNAME
sudo rm "/etc/nginx/sites-enabled/$PROJECTNAME";
sudo ln -s "/etc/nginx/sites-available/$PROJECTNAME" "/etc/nginx/sites-enabled/$PROJECTNAME"

FIRST_INSTALL=0
if ! [ -d $TARGET ]; then
    FIRST_INSTALL=1
    sudo mkdir -p $TARGET
fi

cd $TARGET

if ! [ -d $TARGET/sites ]; then
    sudo drush make -y $SRC/drush.make
fi

if ! [ -a $TARGET/sites/default/settings.php ]; then
    skip=""
    if [ $FIRST_INSTALL -eq 1 ]; then
        skip="-y"
    fi
    sudo drush site-install $skip standard --account-name=admin --account-pass="f!zzyf!zzyf!zzy" --db-url="mysql://$DBUSER:$DBPASS@$DBHOST/$DBNAME"
fi

sudo mkdir "$TARGETROOT/private"
sudo mkdir "$TARGETROOT/tmp"

$SRC/post-drush-site-install

if [ -d $SRC/features ]; then
    echo "Copying features..."
    for f in $SRC/features/*; do
        fname=`basename $f`
        to=$TARGET/modules/$fname
        sudo rm -rf $to
        sudo cp -r $f $to
    done
    echo "...features copied"
fi

fizzy-drupal-install-custom-modules

echo "Copying libraries..."
sudo mkdir -p $TARGET/sites/all/libraries
libs="tinymce ckeditor s3-php5-curl backbone underscore"
for l in $libs; do
    sudo rm -rf  "$TARGET/sites/all/libraries/$l"
    sudo cp -r "$LIB/$l" "$TARGET/sites/all/libraries"
done
echo "... libraries copied"

echo "Copying OwlCarousel..."
sudo rm -rf "$TARGET/sites/all/libraries/owl-carousel"
sudo cp -r "$LIB/OwlCarousel/owl-carousel" "$TARGET/sites/all/libraries"
echo "... done"

echo "Setting up awssdk2..."
sudo rm -rf "$TARGET/sites/all/libraries/awssdk2"
fizzy-lib-awssdk2-install "$TARGET/sites/all/libraries/awssdk2"
echo "... done"

to_install="$(cat $SRC/modules.install) $(cat $SRC/modules.install.d/*)"
to_disable="$(cat $SRC/modules.disable) $(cat $SRC/modules.disable.d/*)"
to_enable="$(cat $SRC/modules.enable) $(cat $SRC/modules.enable.d/*)"

for m in $to_install; do
    echo "Installing $m..."
    m_package=$m
    if echo "$m_package" | grep -q '\-7.x'; then
        m_package=`expr match "$m_package" '\(.*\)-7.x'`
    fi
    m_dir="$TARGET/sites/all/modules/$m_package"
    if ! [ -d $m_dir ]; then
        echo "Downloading $m"
        sudo drush dl -n $m
    else
        echo "$m already downloaded"
    fi
    echo "... $m installed"
done

sudo drush en -y $to_enable
sudo drush dis -y $to_disable

sudo rm $TARGET/robots.txt

sudo chown -R www-data: $TARGETROOT
sudo chmod 644 $TARGET/sites/default/files/.htaccess

sudo cp $SRC/php-settings.ini /etc/php5/apache2/conf.d/php-settings.ini
sudo cp $SRC/php-settings.ini /etc/php5/fpm/conf.d/php-settings.ini

$DIR/build
sudo drush features-revert-all -y
fizzy-drupal-drush-extra-default
$SRC/drush-extra
sudo drush updatedb -y
sudo drush cc all

if ! sudo php5-fpm -t; then
    echo "php5-fpm failed config test. Bailing."
    exit 1
fi
if ! sudo service php5-fpm reload; then
    sudo pkill php5-fpm;
    sudo service php5-fpm start
fi

if ! sudo nginx -t; then
    echo "nginx failed config test. Bailing."
    exit 1
fi
sudo service nginx reload
